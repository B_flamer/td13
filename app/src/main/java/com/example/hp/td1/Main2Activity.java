package com.example.hp.td1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class Main2Activity extends AppCompatActivity {

    private EditText nb1,nb2;
    private TextView tv1,tv2,tv3,tv4;
    private Button but,nex;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        nb1=(EditText)findViewById(R.id.num1);
        nb2=(EditText)findViewById(R.id.num2);
        tv1=(TextView)findViewById(R.id.TV1);
        tv2=(TextView)findViewById(R.id.TV2);
        tv3=(TextView)findViewById(R.id.Tv3);
        tv4=(TextView)findViewById(R.id.TV4);
        but=(Button)findViewById(R.id.But);
        nex=(Button)findViewById(R.id.next);
        nex.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent1 = new Intent(Main2Activity.this,Main3Activity.class);
                startActivity(intent1);

            }
        });
        but.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                operation(v);
            }
        });


    }

    public void operation(View V){

        int num1;
        int num2;
        int res1,res2,res3,res4;
        num1=Integer.parseInt(nb1.getText().toString());
        num2=Integer.parseInt(nb2.getText().toString());
        res1=num1+num2;
        res2=num1-num2;
        res3=num1*num2;
        if(num2==0){
            tv4.setText("Error");
        }
        else { res4=num1/num2;
            tv4.setText(String.valueOf(res4));
        }
        tv1.setText(String.valueOf(res1));
        tv2.setText(String.valueOf(res2));
        tv3.setText(String.valueOf(res3));


    }
}
