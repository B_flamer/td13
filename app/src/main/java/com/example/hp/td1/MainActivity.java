package com.example.hp.td1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    private EditText etGauche;
    private EditText etDroite;
    private TextView tvResult;
    private Button mButton,mb2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        etGauche=(EditText)findViewById(R.id.editText2);
        etDroite=(EditText)findViewById(R.id.editText4);
        tvResult=(TextView)findViewById(R.id.textView2);
        mButton=(Button)findViewById(R.id.button);
        mb2=(Button)findViewById(R.id.button2);
        mb2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(MainActivity.this,Main2Activity.class);
                startActivity(intent);
            }
        });
        mButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                computeAdd(v);
            }
        });
    }
    public void computeAdd(View V){

        int num1;
        int num2;
        int res;
        num1=Integer.parseInt(etGauche.getText().toString());
        num2=Integer.parseInt(etDroite.getText().toString());
        res=num1+num2;
        tvResult.setText(String.valueOf(res));
    }
}
