package com.example.hp.td1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.w3c.dom.Text;

public class CalculatriceActivity extends AppCompatActivity {

    private TextView scn;
    private Button btn1,btn2,btn3,btn4,btn5,btn6,
    btn7,btn8,btn9,btn0,btnPlus,btnMin,btnEqual,
    btnMult,btnDiv,btnNg,btnRes,btnOp,btnOp1;
    private double temp;
    //boolean operator=false,evaluate=false;
    private String str="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.calculatrice);

        scn=(TextView)findViewById(R.id.screen);
        btn0=(Button)findViewById(R.id.bnt0);
        btn1=(Button)findViewById(R.id.bnt1);
        btn2=(Button)findViewById(R.id.bnt2);
        btn3=(Button)findViewById(R.id.bnt3);
        btn4=(Button)findViewById(R.id.bnt4);
        btn5=(Button)findViewById(R.id.bnt5);
        btn6=(Button)findViewById(R.id.bnt6);
        btn7=(Button)findViewById(R.id.bnt7);
        btn8=(Button)findViewById(R.id.bnt8);
        btn9=(Button)findViewById(R.id.bnt9);
        btnPlus=(Button)findViewById(R.id.bntPlus);
        btnMin=(Button)findViewById(R.id.bntMinus);
        btnEqual=(Button)findViewById(R.id.bntEqual);
        btnMult=(Button)findViewById(R.id.bntMult);
        btnDiv=(Button)findViewById(R.id.bntDiv);
        btnNg=(Button)findViewById(R.id.bntNeg);
        btnRes=(Button)findViewById(R.id.bntReset);
        btnOp=(Button)findViewById(R.id.bntOp1);
        btnOp1=(Button)findViewById(R.id.bntOp2);
        btn0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                scn.setText(scn.getText()+"0");
            }
        });
        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                scn.setText(scn.getText()+"1");
            }
        });
        btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                scn.setText(scn.getText()+"2");
            }
        });
        btn3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                scn.setText(scn.getText()+"3");
            }
        });
        btn4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                scn.setText(scn.getText()+"4");
            }
        });
        btn5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                scn.setText(scn.getText()+"5");
            }
        });
        btn6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                scn.setText(scn.getText()+"6");
            }
        });
        btn7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                scn.setText(scn.getText()+"7");
            }
        });
        btn8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                scn.setText(scn.getText()+"8");
            }
        });
        btn9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                scn.setText(scn.getText()+"9");
            }
        });
        btnPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                scn.setText(scn.getText()+"+");
    }
        });
        btnMin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                scn.setText(scn.getText()+"-");

            }
        });
        btnMult.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                scn.setText(scn.getText()+"*");

            }
        });
        btnDiv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                scn.setText(scn.getText()+"/");

            }
        });
        btnEqual.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                double res=0.0;
                str=str+scn.getText();
                res=eval(str);
                scn.setText(String.valueOf(res));
                 }

        });
        btnRes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                scn.setText("");
                str="";
            }
        });
        btnOp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                scn.setText(scn.getText()+"(");
            }
        });
        btnNg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               scn.setText("-"+scn.getText());

            }
        });
        btnOp1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                scn.setText(scn.getText()+")");
            }
        });
    }
    //evaluation de l'entrée
    public static double eval(final String str) {
        return new Object() {
            int pos = -1, ch;

            void nextChar() {
                ch = (++pos < str.length()) ? str.charAt(pos) : -1;
            }

            boolean eat(int charToEat) {
                while (ch == ' ') nextChar();
                if (ch == charToEat) {
                    nextChar();
                    return true;
                }
                return false;
            }

            double parse() {
                nextChar();
                double x = parseExpression();
                if (pos < str.length()) throw new RuntimeException("Unexpected: " + (char)ch);
                return x;
            }

            // Grammar:
            // expression = term | expression `+` term | expression `-` term
            // term = factor | term `*` factor | term `/` factor
            // factor = `+` factor | `-` factor | `(` expression `)`

            double parseExpression() {
                double x = parseTerm();
                for (;;) {
                    if      (eat('+')) x += parseTerm(); // addition
                    else if (eat('-')) x -= parseTerm(); // subtraction
                    else return x;
                }
            }

            double parseTerm() {
                double x = parseFactor();
                for (;;) {
                    if      (eat('*')) x *= parseFactor(); // multiplication
                    else if (eat('/')) x /= parseFactor(); // division
                    else return x;
                }
            }

            double parseFactor() {
                if (eat('+')) return parseFactor(); // unary plus
                if (eat('-')) return -parseFactor(); // unary minus

                double x;
                int startPos = this.pos;
                if (eat('(')) { // parentheses
                    x = parseExpression();
                    eat(')');
                } else if ((ch >= '0' && ch <= '9')) { // numbers
                    while ((ch >= '0' && ch <= '9')) nextChar();
                    x = Double.parseDouble(str.substring(startPos, this.pos));
                } else {
                    throw new RuntimeException("Unexpected: " + (char)ch);
                }

                return x;
            }
        }.parse();
    }


}
